PROJECT_VAR_DIRECTORY= ../
PROJECT_LOG_DIRECTORY= ../logs/

bash wget_model.sh

if [[ -f name_gender.pid ]]; then
    echo "It appears that a pid file is already present; clean it to be able to launch the server again."
    exit 1
fi

gunicorn -c gunicorn_conf.py "wsgi:app"
echo "Launched server on port 7000"

