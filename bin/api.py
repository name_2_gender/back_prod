# coding=utf-8


########################################################################################################################
########################################################################################################################
########################################################################################################################


import sys
import ast
import json
import datetime
from socket import *
from flask import jsonify
from flask_cors import CORS
from flask import Flask,request
from sklearn.externals import joblib
from flask_restplus import Api,Resource

sys.path.insert(0,'../src')
from config import GLOBAL_PATH
from name import Name
import preprocess


########################################################################################################################
########################################################################################################################
########################################################################################################################


model = joblib.load(GLOBAL_PATH+"models/name_to_gender_model.jb")


# A NE PAS CHANGER
sock = socket()
sock.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
app = Flask(__name__)
CORS(app)
api = Api(app)


########################################################################################################################
########################################################################################################################
########################################################################################################################


@api.route('/predict_gender')
class Gender(Resource):
    def get(self):
        name = request.args.get('name')
        name = Name(name.lower())
        result = 'G' if name.set_gender(model) == 0 else 'F'
        return result


@api.route('/valid_gender')
class Gender(Resource):
    def get(self):
        name = request.args.get('name')
        valid = request.args.get('true')
        prediction = request.args.get('pred')
        feedback = "\n%s;%s;%s;%s" % (datetime.datetime.today().strftime('%d-%m-%Y'),
                                        name,
                                        str(prediction),
                                        str(valid))
        with open(GLOBAL_PATH + "data/feedbacks/name_feedbacks.csv", "a") as f:
            f.write(feedback)
        return 'good'



########################################################################################################################
########################################################################################################################
########################################################################################################################


if __name__ == "__main__":
    app.run()

