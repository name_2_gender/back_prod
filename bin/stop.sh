if [[ ! -f ./name_gender.pid ]]; then
    echo "The pid file does't exist, the application wasn't launch"
    exit 1
fi

cat ./name_gender.pid | xargs kill
rm ./name_gender.pid
echo "Cleaned pid and kill server"

