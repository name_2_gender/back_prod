# coding=utf-8


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


import math
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.base import TransformerMixin, BaseEstimator
from config import GLOBAL_PATH, timeit, check_last_index


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


def create_dict_char_position(n_char=1):
    """
    This function returns a transformation dictionnary to create increment a vector
    based on letters in a name. a = position 1 in a vector of length 26
    :param n_char: int
    :return: dict
    """
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    if n_char == 1:
        chars = [letter for letter in alphabet]
        transform_dict = {chars[i]: i for i in range(len(chars))}
        return transform_dict
    elif n_char == 2:
        chars = []
        for letter_1 in alphabet:
            for letter_2 in alphabet:
                chars.append(letter_1 + letter_2)
        transform_dict = {chars[i]: i for i in range(len(chars))}
        return transform_dict
    else:
        raise ValueError


def first_and_last_letter(name, n_char=1):
    """
    This function transform a name into a vector. It considers the name first and last letter
    :param name: string
    :param n_char: int
    :return: List
    """
    if n_char <= 2:
        first_letter = np.zeros(int(math.pow(26, n_char)))
        last_letter = np.zeros(int(math.pow(26, n_char)))
        transform_dict = create_dict_char_position(n_char)
        try:
            first_letter[transform_dict[name[0:n_char]]] += 1
        except:
            pass
        try:
            last_letter[transform_dict[name[-n_char:]]] += 1
        except:
            pass
        return [*first_letter, *last_letter]
    else :
        raise ValueError


def all_letters(name, n_char=1):
    """
    This function transform a name into a vector. It considers all the letters of the name
    :param name: string
    :param n_char : Int
    :return: List
    """
    name_chars = np.zeros(26)
    transform_dict = create_dict_char_position(n_char)
    for char in name:
        try:
            name_chars[transform_dict[char]] += 1
        except:
            pass
    return name_chars


def create_train_test_set(df, split_rate=0.8):
    """
    This function takes our dataset and creates a training and testing test
    :param df: Pandas df
    :param split_rate: float between 0.0 and 1.0
    :return: list, list, list, list
    """
    names = list(df['name'])
    genders = df['gender']
    genders = [0 if gender is 'M' else 1 for gender in genders]
    x_train, x_test, y_train, y_test = train_test_split(names, genders, test_size=split_rate)
    return x_train, x_test, y_train, y_test


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


class FirstLastLetterTransformer(BaseEstimator, TransformerMixin):
    """
    Specific transformer Class for pipelines
    Constructor : need param n_char (default to 1)
    """
    def __init__(self, n_char=1):
        self.n_char = n_char

    def fit(self, X, y=None):
        return self

    @timeit
    def transform(self, X, y=None):
        return [first_and_last_letter(name, self.n_char) for name in X]


class AllLettersTransformer(BaseEstimator, TransformerMixin):
    """
    Specific transformer Class for pipelines
    Constructor : need param n_char (default to 1)
    """
    def __init__(self, n_char=1):
        self.n_char = n_char

    def fit(self, X, y=None):
        return self

    @timeit
    def transform(self, X, y=None):
        return [all_letters(name, self.n_char) for name in X]

