# coding=utf-8

#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

import time
import datetime
import logging
import re
import os

#######################################################################################################################

file_path = os.getcwd()
GLOBAL_PATH = os.path.abspath(os.path.join(file_path, '..'))+'/'

#######################################################################################################################

formatter = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s")

handler_critic = logging.FileHandler(GLOBAL_PATH+"logs/critic.log", mode="a", encoding="utf-8")
handler_info = logging.FileHandler(GLOBAL_PATH+"logs/info.log", mode="a", encoding="utf-8")

handler_critic.setFormatter(formatter)
handler_info.setFormatter(formatter)

handler_info.setLevel(logging.INFO)
handler_critic.setLevel(logging.CRITICAL)

logger = logging.getLogger("name_gender")
logger.setLevel(logging.INFO)
logger.addHandler(handler_critic)
logger.addHandler(handler_info)

#######################################################################################################################

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        temps_reponse = "\n%s;%s;%s" % (datetime.datetime.today().strftime('%d-%m-%Y'),
                                        method.__name__,
                                        te-ts)
        with open(GLOBAL_PATH + "logs/temps_reponse.csv", "a") as f:
            f.write(temps_reponse)
        return result
    return timed


def check_last_index(folder):
    """
    This function returns the max index observed in files name
    It would be used to know how to name next files
    :param folder: folder containing files to process
    :return: max_index : int
    """
    files = os.listdir(folder)
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    if len(files) == 0:
        return -1
    else:
        indexes = [int(re.sub("[^0-9^]", "", file)) for file in files]
        return max(indexes)


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
