Architecture de dossiers à récupérer sur les projets

pour lancer le service d'API test, à la récupération de ce projet , il faut : 

 - changer les variables suivantes :
 	- dans /src/config.py:
		- GLOBAL_PATH : remplacer la variable par le chemin de votre dossier flask_backoffice dans votre local
	- dans /bin/gunicorn_conf.py:
		- pidfile : remplacer par le nom de votre projet. Exemple : hotliner
	- dans /bin/stop.sh:
		- remplacer "nom_projet" par le nom de  votre projet
	- dans /bin/start.sh:
		- remplacer "nom_projet" par le nom de votre projet


Une fois ces  modifications faites, "bash start.sh" vous permettra de lancer vos API,"bash stop.sh"  de les  arrêter
